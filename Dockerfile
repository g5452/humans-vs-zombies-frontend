# STAGE 1
FROM node:12-alpine AS build

WORKDIR /app
COPY package.json ./
RUN npm install
COPY . /app
RUN yarn build
#CMD ["npm", "start"]

# STAGE 2
FROM node:12-alpine
WORKDIR /app
RUN npm install -g webserver.local
COPY --from=build /app/build ./build
CMD webserver.local -d ./build