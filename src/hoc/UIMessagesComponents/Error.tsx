const Error = ({message}: any) => {
    return(
        <div className='alert alert-danger m-4 p-1'>
            <h5>Well this is embarrassing...</h5>
            <p>An error occurred: <br/>{ message }</p>
        </div>
    )
}

export default Error