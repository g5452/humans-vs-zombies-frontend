import { useState } from "react";
import { Collapse } from 'react-bootstrap';

const CollapseCard = ({title, children, show} : any) => {
    const willDisplay = () : boolean => {
        if (show) return show;
        else return false
    }
    const [open, setOpen] = useState(willDisplay);

    return (
        <div className="card">
            <div className="card-body">
                <div onClick={()=> setOpen(!open)} 
                    aria-controls="collapse-element"
                    aria-expanded={open}>  
                        <div className="h5 card-title"> {title} </div>
                </div> 
                <Collapse in={open}>
                    <div id="collapse-element">
                        {children}
                    </div>
                </Collapse>
            </div>
        </div>
    )
}

export default CollapseCard;