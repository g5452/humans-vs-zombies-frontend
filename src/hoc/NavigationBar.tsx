
const NavigationBar = (props: any) => {

    return (
        <nav className="navbar Nav-bar mb-3">
            <div className="container-fluid">
                <div className="navbar-header">
                    <div className="navbar-brand"><img src="../../favicon.png" alt="icon"></img> <div className='nav-link text-white Nav-bar-header'>Humans Vs Zombies</div></div>
                </div>
                {props.children}
            </div>
        </nav>
    )
}

export default NavigationBar;