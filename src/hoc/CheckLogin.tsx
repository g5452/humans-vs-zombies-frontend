import { useAuth0 } from "@auth0/auth0-react"
import { Link, Navigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../hooks";

/**
 * Checks if Auth0 session is active, but state session is not, then dispatches 
 * login action to set session state if not.
 */
const LoginCheck= () => {
    const dispatch = useAppDispatch();
    const { user, isAuthenticated } = useAuth0();
    const { loggedIn } = useAppSelector(state => state.session);

    return (
        <>
        { !loggedIn && 
            <Navigate to="/"></Navigate>
        }
        </>
    )
}

export default LoginCheck