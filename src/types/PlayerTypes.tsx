export interface IPlayer{
    id: number,
    isHuman: boolean,
    isPatientZero: boolean,
    biteCode: number,
    userId: string,
    gameId: number
}

export interface RegisterPlayerCredentials {
    isHuman?: boolean,
    isPatientZero?: boolean,
    userId?: string,
    gameId?: number
}

export interface GetPlayerCredentials {
    id: number
}

export enum PlayerState {
    DEFAULT = "-- Change Player Status --",
    HUMAN = "Human",
    ZOMBIE = "Zombie"
}

export interface IEditPlayer{
    isHuman: boolean,
    isPatientZero: boolean
}
export interface ISessionPlayer {
    playerId: number | undefined,
    isHuman: boolean | undefined,
    isPatientZero: boolean | undefined,
    biteCode: string | undefined,
    userId: string | undefined,
    gameId: number | undefined,
    squadId: number | undefined
}
