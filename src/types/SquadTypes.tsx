export interface ISquad {
    id: number,
    name: string,
    isHuman: boolean,
    gameId: number,
    squadMembers: number[] | null,
}

export interface ICreateSquad {
    name: string,
    isHuman: boolean,
    gameId: number
}

export interface IUpdateSquad {
    name: string,
    isHuman: boolean,
    gameId: number,
}

export enum SquadState {
    DEFAULT = "-- Squad Status --",
    HUMAN = "Human",
    ZOMBIE = "Zombie"
}