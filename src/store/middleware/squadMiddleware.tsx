import SquadAPI from "../../components/api/SquadAPI";
import { AnyAction, Middleware } from 'redux';
import { ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_ATTEMPTING, ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_SUCCESS, ACTION_SQUADMEMBER_GETBYSQUADID_ATTEMPTING, ACTION_SQUADMEMBER_GETBYSQUADID_SUCCESS, ACTION_SQUADS_GETBYGAMEID_ATTEMPTING, ACTION_SQUADS_GETBYGAMEID_SUCCESS, ACTION_SQUAD_CREATE_ATTEMPTING, ACTION_SQUAD_CREATE_SUCCESS, ACTION_SQUAD_GETALL_ATTEMPTING, ACTION_SQUAD_GETALL_SUCCESS, ACTION_SQUAD_GETBYID_ATTEMPTING, ACTION_SQUAD_GETBYID_SUCCESS,  ACTION_SQUAD_GETBYPLAYERID_ATTEMPTING,  ACTION_SQUAD_GETBYPLAYERID_SUCCESS,  killedMembersFetchByIdAttemptingAction,  killedMembersFetchByIdErrorAction,  killedMembersFetchByIdSuccessAction,  squadFetchByIdErrorAction,  squadFetchByIdSuccessAction,  squadFetchByPlayerIdAttemptingAction,  squadFetchByPlayerIdErrorAction,  squadFetchByPlayerIdSuccessAction,  squadMemberInSquadFetchByIdAttemptingAction,  squadMemberInSquadFetchByIdCompleteAction, squadMemberInSquadFetchByIdErrorAction, squadMemberInSquadFetchByIdSuccessAction, squadsCreateCompleteAction, squadsCreateErrorAction, squadsCreateSuccessAction, squadsFetchAttemptingAction, squadsFetchCompleteAction, squadsFetchErrorAction, squadsFetchSuccessAction, squadsInGameFetchByIdErrorAction, squadsInGameFetchByIdSuccessAction } from "../actions/squadActions";
import { ACTION_SQUADMEMBER_CREATE_ATTEMPTING, ACTION_SQUADMEMBER_CREATE_SUCCESS, ACTION_SQUADMEMBER_GETBYID_SUCCESS } from "../actions/squadMemberActions";
import { killedSessionSetAction, squadmemberSessionSetAction } from "../actions/sessionActions";

export const getAllSquadsMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_SQUAD_GETALL_ATTEMPTING) {
        SquadAPI.getAll()
            .then(response => {
                dispatch(squadsFetchSuccessAction(response.data))
            }).catch(error => {
                dispatch(squadsFetchErrorAction(error.message))
            })
    }
    if (action.type === ACTION_SQUAD_GETALL_SUCCESS) {
        //localStorage.setItem('squads', JSON.stringify(action.payload));
        //dispatch(squadsFetchCompleteAction());
    }
}

export const getSquadMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_SQUAD_GETBYID_ATTEMPTING) {
        SquadAPI.get(action.payload)
            .then(response => {
                dispatch(squadFetchByIdSuccessAction(response.data))
            }).catch(error => {
                dispatch(squadFetchByIdErrorAction(error.message))
            })
    }
    if (action.type === ACTION_SQUAD_GETBYID_SUCCESS) {
        
    }
}

export const createSquadMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_SQUAD_CREATE_ATTEMPTING) {
        SquadAPI.create(action.payload, action.playerID)
            .then(response => {
                dispatch(squadsCreateSuccessAction(response.data, action.playerID))
            }).catch(error => {
                dispatch(squadsCreateErrorAction(error.message))
            })
    }
    if (action.type === ACTION_SQUAD_CREATE_SUCCESS) {
        dispatch(squadFetchByPlayerIdAttemptingAction(action.playerID));
    }
}

export const getSquadMemberInSquadMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)
    
    if (action.type === ACTION_SQUADMEMBER_GETBYSQUADID_ATTEMPTING) {
        SquadAPI.getAllMembers(action.payload)
            .then(response => {
                dispatch(squadMemberInSquadFetchByIdSuccessAction(response.data))
            }).catch(error => {
                dispatch(squadMemberInSquadFetchByIdErrorAction(error.message))
            })
    }
    if (action.type === ACTION_SQUADMEMBER_GETBYSQUADID_SUCCESS) {
        //dispatch(squadmemberSessionSetAction(action.payload))
    }
}

export const getSquadsInGameMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)
    if (action.type === ACTION_SQUADS_GETBYGAMEID_ATTEMPTING) {
        SquadAPI.getAllSquads(action.payload)
            .then(response => {
                dispatch(squadsInGameFetchByIdSuccessAction(response.data))
            }).catch(error => {
                dispatch(squadsInGameFetchByIdErrorAction(error.message))
            })
    }
    if (action.type === ACTION_SQUADS_GETBYGAMEID_SUCCESS) {
        //localStorage.setItem('squadmember', JSON.stringify(action.payload));
        //dispatch(squadMemberInSquadFetchByIdCompleteAction())
        //dispatch (killedMembersFetchByIdAttemptingAction(action.payload.id))
        //console.log(action.payload.id)
    }
}

export const getKilledMembersMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)
    
    if (action.type === ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_ATTEMPTING) {
        SquadAPI.getKilledMembers(action.payload)
            .then(response => {
                dispatch(killedMembersFetchByIdSuccessAction(response.data))
            }).catch(error => {
                dispatch(killedMembersFetchByIdErrorAction(error.message))
            })
    }
    if (action.type === ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_SUCCESS) {
        console.log("Killed members:") 
        console.log(action.payload)
        dispatch (killedSessionSetAction(action.payload))
    }
}

export const getSquadByPlayerMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_SQUAD_GETBYPLAYERID_ATTEMPTING) {
        SquadAPI.getSquadByPlayerId(action.payload)
            .then(response => {
                dispatch(squadFetchByPlayerIdSuccessAction(response.data))
                dispatch( squadMemberInSquadFetchByIdAttemptingAction(response.data.id))
            }).catch(error => {
                dispatch(squadFetchByPlayerIdErrorAction(error.message))
            })
    }
    if (action.type === ACTION_SQUAD_GETBYPLAYERID_SUCCESS) {
    }
}