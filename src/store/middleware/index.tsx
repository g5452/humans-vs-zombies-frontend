import { applyMiddleware } from "redux";
import { getGameReducer } from "../reducers/gameReducer";
import { createGameMiddleware, getAllGamesMiddleware, getGameMiddleware, updateGameMiddleware } from "./gameMiddleware";
import { registerKillMiddleware } from "./killMiddleware";
import { createPlayerMiddleware, deletePlayerMiddleware, getAllPlayerInGameMiddleware, getPlayerMiddleware, updatePlayerMiddleware } from "./playerMiddleware";
import { createMissionMiddleware, deleteMissionMiddleware, getAllMissionsByGameIdMiddleware, getAllMissionsMiddleware,  } from "./missionMiddleware";
import { loginMiddleware, registerUserMiddleware } from "./userMiddleware";
import { createSquadMiddleware, getAllSquadsMiddleware, getKilledMembersMiddleware, getSquadMemberInSquadMiddleware, getSquadMiddleware, getSquadsInGameMiddleware, getSquadByPlayerMiddleware} from "./squadMiddleware";
import { createSquadMemberMiddleware,  deleteSquadMemberMiddleware,  getSquadMemberMiddleware } from "./squadMemberMiddleware";

// Bundle all Middleware files
export default applyMiddleware(
    // user
    loginMiddleware,
    registerUserMiddleware,
    // games
    getAllGamesMiddleware,
    getGameMiddleware,
    createGameMiddleware,
    updateGameMiddleware,
    //player
    createPlayerMiddleware,
    updatePlayerMiddleware,
    deletePlayerMiddleware,
    getPlayerMiddleware,
    getAllPlayerInGameMiddleware,
    // kill
    registerKillMiddleware,
    // mission
    createMissionMiddleware,
    getAllMissionsMiddleware,
    getAllMissionsByGameIdMiddleware,
    getAllMissionsMiddleware,
    getAllMissionsByGameIdMiddleware,
    deleteMissionMiddleware,
    
    // squads
    // getAllSquadsMiddleware,
    // getSquadMiddleware,
    createSquadMiddleware,
    getSquadMemberInSquadMiddleware,
    getSquadsInGameMiddleware,
    getKilledMembersMiddleware,
    //updateSquadMiddleware,

    getSquadMemberMiddleware,
    createSquadMemberMiddleware,
    getSquadByPlayerMiddleware,
    deleteSquadMemberMiddleware,
    //updateSquadMemberMiddleware
)