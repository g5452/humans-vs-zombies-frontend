import { AnyAction, Middleware } from "redux"
import PlayerAPI from "../../components/api/PlayerAPI"
import { gameFetchAttemptAction, gamesFetchAttemptAction } from "../actions/gameActions"
import { ACTION_PLAYERINGAME_GET_ATTEMPTING, ACTION_PLAYERINGAME_GET_SUCCESS, ACTION_PLAYER_CREATE_ATTEMPTING, ACTION_PLAYER_CREATE_SUCCESS, 
    ACTION_PLAYER_DELETE_ATTEMPTING, 
    ACTION_PLAYER_DELETE_SUCCESS, 
    ACTION_PLAYER_GET_ATTEMPTING, ACTION_PLAYER_GET_SUCCESS, ACTION_PLAYER_UPDATE_ATTEMPTING, ACTION_PLAYER_UPDATE_SUCCESS, playerCreateCompleteAction, 
    playerCreateErrorAction, playerCreateSuccessAction, playerDeleteCompleteAction, playerDeleteErrorAction, playerDeleteSuccessAction, playerGetCompleteAction, 
    playerGetErrorAction, playerGetSuccessAction, playerInGameGetAttemptAction, playerInGameGetCompleteAction, playerInGameGetErrorAction, playerInGameGetSuccessAction, playerUpdateCompleteAction, playerUpdateErrorAction, playerUpdateSuccessAction } from "../actions/playerActions"
import { playerSessionSetAction, playerSessionClearAction,  } from "../actions/sessionActions"
import { loginAttemptAction } from "../actions/userActions"


export const createPlayerMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_PLAYER_CREATE_ATTEMPTING) {
        PlayerAPI.create(action.payload)
        .then(response => {
            dispatch( playerCreateSuccessAction(response.data) )
        }).catch(error => {
            dispatch( playerCreateErrorAction(error.message) )
        })
    }
    if (action.type === ACTION_PLAYER_CREATE_SUCCESS) {
        dispatch( playerSessionSetAction(action.payload) )
        // must update gamelist:
        dispatch( gamesFetchAttemptAction() )
        dispatch(gameFetchAttemptAction(action.payload.gameId));
        dispatch(loginAttemptAction(action.payload.userId));
        dispatch( playerInGameGetAttemptAction(action.payload.gameId));
    }
}

export const updatePlayerMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)
    if(action.type === ACTION_PLAYER_UPDATE_ATTEMPTING){
        console.log("response")
        PlayerAPI.update(action.current, action.payload)
        .then(response => {
            console.log(response)
            dispatch( playerUpdateSuccessAction(action.payload.id) )
  
        }).catch(error => {
            dispatch( playerUpdateErrorAction(error.message) )
        })
    }

    if (action.type === ACTION_PLAYER_UPDATE_SUCCESS) {
    }
}
      
export const getPlayerMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_PLAYER_GET_ATTEMPTING) {
        PlayerAPI.get(action.payload)
        .then(response => {
            dispatch( playerGetSuccessAction(response.data) )
        }).catch(error => {
            dispatch( playerGetErrorAction(error.message) )
        })
    }
    if (action.type === ACTION_PLAYER_GET_SUCCESS) {
        dispatch( playerSessionSetAction(action.payload) )
        dispatch( playerGetCompleteAction() )
    }
}

export const getAllPlayerInGameMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_PLAYERINGAME_GET_ATTEMPTING) {
        PlayerAPI.getAllPlayersInGame(action.payload)
        .then(response => {
            dispatch( playerInGameGetSuccessAction(response.data) )
        }).catch(error => {
            dispatch( playerInGameGetErrorAction(error.message) )
        })
    }
    if (action.type === ACTION_PLAYERINGAME_GET_SUCCESS) {
    }
}

export const deletePlayerMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_PLAYER_DELETE_ATTEMPTING) {
        PlayerAPI.remove(action.payload)
        .then(response => {
            dispatch( playerDeleteSuccessAction(response.data) )
        }).catch(error => {
            dispatch( playerDeleteErrorAction(error.message) )
        })
    }
    if (action.type === ACTION_PLAYER_DELETE_SUCCESS) {
        dispatch( playerSessionClearAction() );
        dispatch( playerDeleteCompleteAction() )
        dispatch(gamesFetchAttemptAction());
    }
}