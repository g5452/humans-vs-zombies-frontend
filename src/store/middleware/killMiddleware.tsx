import { AnyAction } from "redux"
import { Middleware } from "redux"
import KillAPI from "../../components/api/KillAPI"
import { ACTION_KILL_REGISTER_ATTEMPTING, ACTION_KILL_REGISTER_SUCCESS, killRegisterCompleteAction, killRegisterErrorAction, killRegisterSuccessAction } from "../actions/killActions"

export const registerKillMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_KILL_REGISTER_ATTEMPTING) {
        KillAPI.registerKillAndUpdatePlayer(action.payload)
        .then(response => {
            dispatch( killRegisterSuccessAction(response.data) )
        }).catch(error => {
            dispatch( killRegisterErrorAction(error.message) )
        })
    }
    if (action.type === ACTION_KILL_REGISTER_SUCCESS) {
    }
}
