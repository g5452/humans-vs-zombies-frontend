// create player
import { IRegisterKill, IKill } from "../../types/KillTypes"

export const ACTION_KILL_REGISTER_ATTEMPTING = '[register] kill ATTEMPT'
export const ACTION_KILL_REGISTER_SUCCESS    = '[register] kill SUCCESS'
export const ACTION_KILL_REGISTER_ERROR      = '[register] kill ERROR'
export const ACTION_KILL_REGISTER_COMPLETE   = '[register] kill COMPLETE'

export const killRegisterAttemptAction = (registerKill : IRegisterKill) => ({
    type: ACTION_KILL_REGISTER_ATTEMPTING,
    payload: registerKill
})

export const killRegisterSuccessAction = (kill : IKill) => ({
    type: ACTION_KILL_REGISTER_SUCCESS,
    payload: kill
})

export const killRegisterErrorAction = (message : string) => ({
    type: ACTION_KILL_REGISTER_ERROR,
    payload: message
})

export const killRegisterCompleteAction = () => ({
    type: ACTION_KILL_REGISTER_COMPLETE
})