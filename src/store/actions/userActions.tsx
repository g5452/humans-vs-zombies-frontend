import { IEditUser, IUser} from "../../types/UserTypes";

// login
export const ACTION_LOGIN_ATTEMPTING = '[login] ATTEMPT'
export const ACTION_LOGIN_SUCCESS = '[login] SUCCESS'
export const ACTION_LOGIN_ERROR = '[login] ERROR'

export const loginAttemptAction = (credentials : string) => ({
    type: ACTION_LOGIN_ATTEMPTING,
    payload: credentials
})

export const loginSuccessAction = (profile : IUser) => ({
    type: ACTION_LOGIN_SUCCESS,
    payload: profile
})

export const loginErrorAction = (message : string) => ({
    type: ACTION_LOGIN_ERROR,
    payload: message
})


// register
export const ACTION_REGISTER_ATTEMPTING = '[register] ATTEMPT'
export const ACTION_REGISTER_SUCCESS = '[register] SUCCESS'
export const ACTION_REGISTER_ERROR = '[register] ERROR'
export const ACTION_REGISTER_RESET = '[register user] RESET'

export const registerAttemptAction = (credentials : IEditUser) => ({
    type: ACTION_REGISTER_ATTEMPTING,
    payload: credentials
})

export const registerSuccessAction = (profile : IUser) => ({
    type: ACTION_REGISTER_SUCCESS,
    payload: profile
})

export const registerErrorAction = (message : string) => ({
    type: ACTION_REGISTER_ERROR,
    payload: message
})

export const registerResetAction = () => ({
    type: ACTION_REGISTER_RESET
})