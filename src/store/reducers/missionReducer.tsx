import { AnyAction } from "redux";
import { IMission } from "../../types/MissionTypes";
import { 
    // GetAll
    ACTION_MISSION_GETALL_ATTEMPTING, 
    ACTION_MISSION_GETALL_COMPLETE, 
    ACTION_MISSION_GETALL_ERROR, 
    ACTION_MISSION_GETALL_SUCCESS,
    // GetAll by GameId
    ACTION_MISSION_GETALLBYGAMEID_ATTEMPTING, 
    ACTION_MISSION_GETALLBYGAMEID_COMPLETE, 
    ACTION_MISSION_GETALLBYGAMEID_ERROR, 
    ACTION_MISSION_GETALLBYGAMEID_SUCCESS,
    // Create
    ACTION_MISSION_CREATE_ATTEMPTING, 
    ACTION_MISSION_CREATE_COMPLETE, 
    ACTION_MISSION_CREATE_ERROR, 
    ACTION_MISSION_CREATE_SUCCESS,
    // Delete
    ACTION_MISSION_DELETE_ATTEMPTING, 
    ACTION_MISSION_DELETE_COMPLETE, 
    ACTION_MISSION_DELETE_ERROR, 
    ACTION_MISSION_DELETE_SUCCESS,
} from "../actions/missionActions"

// Get all
interface GetAllSMission {
    getAllMissionAttempting: boolean,
    getAllMissionError: string,
    getAllMissionSuccess: boolean,
    allMissions : IMission[]
}

const initialGetAllMission : GetAllSMission = {
    getAllMissionAttempting: false,
    getAllMissionError: "",
    getAllMissionSuccess: false,
    allMissions : []
}

export const getAllMissionsReducer = ( state = initialGetAllMission, action : AnyAction) => {
    switch (action.type) {
        case ACTION_MISSION_GETALL_ATTEMPTING:
            return {
                ...state,
                getAllMissionAttempting: true,
                getAllMissionError: "",
                getAllMissionSuccess: false,
                allMissions : []
            }
        case ACTION_MISSION_GETALL_COMPLETE:
            return {
                ...initialGetAllMission
            }
        case ACTION_MISSION_GETALL_ERROR:
            return {
                ...state,
                getAllMissionAttempting: false,
                getAllMissionError: action.payload,
                getAllMissionSuccess: false,
                allMissions : []
            }
        case ACTION_MISSION_GETALL_SUCCESS:
            return {
                ...state,
                getAllMissionAttempting: false,
                getAllMissionError: "",
                getAllMissionSuccess: true,
                allMissions : [...action.payload]
            }
        default: 
            return state;
    }
}

// Get all by gameId
interface GetAllByGameIdMission {
    getAllMissionByGameIdAttempting: boolean,
    getAllMissionByGameIdError: string,
    getAllMissionByGameIdSuccess: boolean,
    gameMissions : IMission[]
}

const initialGetAllByGameIdMission : GetAllByGameIdMission = {
    getAllMissionByGameIdAttempting: false,
    getAllMissionByGameIdError: "",
    getAllMissionByGameIdSuccess: false,
    gameMissions : []
}

export const getAllMissionsByGameIdReducer = ( state = initialGetAllByGameIdMission, action : AnyAction) => {
    switch (action.type) {
        case ACTION_MISSION_GETALLBYGAMEID_ATTEMPTING:
            return {
                ...state,
                getAllMissionByGameIdAttempting: true,
                getAllMissionByGameIdError: "",
                getAllMissionByGameIdSuccess: false,
                gameMissions : []
            }
        case ACTION_MISSION_GETALLBYGAMEID_COMPLETE:
            return {
                ...initialGetAllByGameIdMission
            }
        case ACTION_MISSION_GETALLBYGAMEID_ERROR:
            return {
                ...state,
                getAllMissionByGameIdAttempting: false,
                getAllMissionByGameIdError: action.payload,
                getAllMissionByGameIdSuccess: false,
                gameMissions : []
            }
        case ACTION_MISSION_GETALLBYGAMEID_SUCCESS:
            return {
                ...state,
                getAllMissionByGameIdAttempting: false,
                getAllMissionByGameIdError: "",
                getAllMissionByGameIdSuccess: true,
                gameMissions : [...action.payload]
            }
        default: 
            return state;
    }
}
// Create
interface CreateMission {
    createMissionAttempting: boolean,
    createMissionError: string,
    createMissionSuccess: boolean
}

const initialCreateMission : CreateMission = {
    createMissionAttempting: false,
    createMissionError: "",
    createMissionSuccess: false
}

export const createMissionReducer = ( state = initialCreateMission, action : AnyAction) => {
    switch (action.type) {
        case ACTION_MISSION_CREATE_ATTEMPTING:
            return {
                ...state,
                createMissionAttempting: true,
                createMissionError: "",
                createMissionSuccess: false
            }
        case ACTION_MISSION_CREATE_COMPLETE:
            return {
                ...state,
                createMissionAttempting: false,
                createMissionError: "",
                createMissionSuccess: true
            }
        case ACTION_MISSION_CREATE_ERROR:
            return {
                ...state,
                createMissionAttempting: false,
                createMissionError: action.payload,
                createMissionSuccess: false
            }
        case ACTION_MISSION_CREATE_SUCCESS:
            return {
                ...initialCreateMission
            }
        default:
            return state;
    }
}
// Delete
interface DeleteMission {
    deleteMissionAttempting: boolean,
    deleteMissionError: string,
    deleteMissionSuccess: boolean
}

const initialDeleteMission : DeleteMission = {
    deleteMissionAttempting: false,
    deleteMissionError: "",
    deleteMissionSuccess: false
}
export const deleteMissionReducer = ( state = initialDeleteMission, action : AnyAction) => {
    switch (action.type) {
        case ACTION_MISSION_DELETE_ATTEMPTING:
            return {
                ...state,
                deleteMissionAttempting: true,
                deleteMissionError: "",
                deleteMissionSuccess: false
            }
        case ACTION_MISSION_DELETE_COMPLETE:
            return {
                ...state,
                deleteMissionAttempting: false,
                deleteMissionError: "",
                deleteMissionSuccess: true
            }
        case ACTION_MISSION_DELETE_ERROR:
            return {
                ...state,
                deleteMissionAttempting: false,
                deleteMissionError: action.payload,
                deleteMissionSuccess: false
            }
        case ACTION_MISSION_DELETE_SUCCESS:
            return {
                ...initialDeleteMission
            }
        default:
            return state;
    }
}

// // Update
// export const updateGameReducer = ( state = initialUpdateGameState, action : AnyAction) => {
//     switch (action.type) {
//         case ACTION_GAME_UPDATE_ATTEMPTING:
//             return {
//                 ...state,
//                 updateGameAttempting: true,
//                 updateGameError: "",
//                 updateGameSuccess: false
//             }
//         case ACTION_GAME_UPDATE_SUCCESS:
//             return {
//                 ...state,
//                 updateGameAttempting: false,
//                 updateGameError: "",
//                 updateGameSuccess: true
//             }
//         case ACTION_GAME_UPDATE_ERROR:
//             return {
//                 ...state,
//                 updateGameAttempting: false,
//                 updateGameError: action.payload,
//                 updateGameSuccess: false
//             }
//         case ACTION_GAME_UPDATE_COMPLETE:
//             return {
//                 ...initialUpdateGameState
//             }
//         default:
//             return state;
//     }
// }