import { AnyAction } from "redux";
import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_ERROR, ACTION_LOGIN_SUCCESS, 
    ACTION_REGISTER_ATTEMPTING, ACTION_REGISTER_SUCCESS, ACTION_REGISTER_ERROR, ACTION_REGISTER_RESET } from "../actions/userActions";


// Get user
interface LoginState {
    loginAttempting: boolean,
    loginError: string
}

const initialLoginState : LoginState = {
    loginAttempting: false,
    loginError: ""
}

export const loginReducer = ( state = initialLoginState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_LOGIN_ATTEMPTING:
            return {
                ...state,
                loginAttempting: true
            }
        case ACTION_LOGIN_SUCCESS:
            return {
                ...initialLoginState
            }
        case ACTION_LOGIN_ERROR:
            return {
                ...state,
                loginAttempting: false,
                loginError: action.payload
            }
        default: 
            return state;
    }
}

// Register user

interface RegisterState {
    registerAttempting: boolean,
    registerError: string,
    registerSuccess: boolean
}

const initialRegisterState : RegisterState = {
    registerAttempting: false,
    registerError: "",
    registerSuccess: false
}

export const registerUserReducer = ( state = initialRegisterState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_REGISTER_ATTEMPTING:
            return {
                ...state,
                registerAttempting: true,
                registerSuccess: false,
                registerError: ""
            }
        case ACTION_REGISTER_SUCCESS:
            return {
                ...state,
                registerAttempting: false,
                registerError: "",
                registerSuccess: true
            }
        case ACTION_REGISTER_ERROR:
            return {
                ...state,
                registerAttempting: false,
                registerError: action.payload,
                registerSuccess: false
            }
        case ACTION_REGISTER_RESET:
            return {
                ...initialRegisterState
            }
        default: 
            return state;
    }
}