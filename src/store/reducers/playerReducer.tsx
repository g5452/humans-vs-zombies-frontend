import { AnyAction } from "redux"
import { IPlayer } from "../../types/PlayerTypes"
//import { ACTION_PLAYERS_FETCH_ATTEMPTING, ACTION_PLAYERS_FETCH_COMPLETE, ACTION_PLAYERS_FETCH_ERROR, ACTION_PLAYERS_FETCH_SUCCESS, ACTION_PLAYER_CREATE_ATTEMPTING, ACTION_PLAYER_CREATE_COMPLETE, ACTION_PLAYER_CREATE_ERROR, ACTION_PLAYER_CREATE_SUCCESS, ACTION_PLAYER_FETCH_ATTEMPTING, ACTION_PLAYER_FETCH_COMPLETE, ACTION_PLAYER_FETCH_ERROR, ACTION_PLAYER_FETCH_SUCCESS, ACTION_PLAYER_UPDATE_ATTEMPTING, ACTION_PLAYER_UPDATE_COMPLETE, ACTION_PLAYER_UPDATE_ERROR, ACTION_PLAYER_UPDATE_SUCCESS } from "../actions/playerActions"
import { ACTION_PLAYERINGAME_GET_ATTEMPTING, ACTION_PLAYERINGAME_GET_COMPLETE, ACTION_PLAYERINGAME_GET_ERROR, ACTION_PLAYERINGAME_GET_SUCCESS, ACTION_PLAYER_CREATE_ATTEMPTING, ACTION_PLAYER_CREATE_COMPLETE, ACTION_PLAYER_CREATE_ERROR, 
    ACTION_PLAYER_CREATE_SUCCESS, ACTION_PLAYER_DELETE_ATTEMPTING, ACTION_PLAYER_DELETE_COMPLETE, ACTION_PLAYER_DELETE_ERROR, ACTION_PLAYER_DELETE_SUCCESS, ACTION_PLAYER_GET_ATTEMPTING, ACTION_PLAYER_GET_COMPLETE, 
    ACTION_PLAYER_GET_ERROR, ACTION_PLAYER_GET_SUCCESS, ACTION_PLAYER_UPDATE_ATTEMPTING, ACTION_PLAYER_UPDATE_COMPLETE, ACTION_PLAYER_UPDATE_ERROR, ACTION_PLAYER_UPDATE_SUCCESS } from "../actions/playerActions"

// Create
interface CreatePlayerState {
    createPlayerAttempting: boolean,
    createPlayerError: string,
    createPlayerSuccess: boolean
}

const initialCreatePlayerState : CreatePlayerState = {
    createPlayerAttempting: false,
    createPlayerError: "",
    createPlayerSuccess: false
}

export const createPlayerReducer = ( state = initialCreatePlayerState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_PLAYER_CREATE_ATTEMPTING:
            return {
                ...state,
                createPlayerAttempting: true,
                createPlayerError: "",
                createPlayerSuccess: false
            }
        case ACTION_PLAYER_CREATE_SUCCESS:
            return {
                ...state,
                createPlayerAttempting: false,
                createPlayerError: "",
                createPlayerSuccess: true
            }
        case ACTION_PLAYER_CREATE_ERROR:
            return {
                ...state,
                createPlayerAttempting: false,
                createPlayerError: action.payload,
                createPlayerSuccess: false
            }
        case ACTION_PLAYER_CREATE_COMPLETE:
            return {
                ...initialCreatePlayerState
            }
        default:
            return state;
    }
}

// Update
interface UpdatePlayerState {
    updatePlayerAttempting: boolean,
    updatePlayerError: string,
    updatePlayerSuccess: boolean
}

const initialUpdatePlayerState : UpdatePlayerState = {
    updatePlayerAttempting: false,
    updatePlayerError: "",
    updatePlayerSuccess: false
}

export const updatePlayerReducer = ( state = initialUpdatePlayerState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_PLAYER_UPDATE_ATTEMPTING:
            return {
                ...state,
                updatePlayerAttempting: true,
                updatePlayerError: "",
                updatePlayerSuccess: false
            }
        case ACTION_PLAYER_UPDATE_SUCCESS:
            return {
                ...state,
                updatePlayerAttempting: false,
                updatePlayerError: "",
                updatePlayerSuccess: true
            }
        case ACTION_PLAYER_UPDATE_ERROR:
            return {
                ...state,
                updatePlayerAttempting: false,
                updatePlayerError: action.payload,
                updatePlayerSuccess: false
            }
        case ACTION_PLAYER_UPDATE_COMPLETE:
            return {
                ...initialUpdatePlayerState}
        default:
                return state;
        }
    }
        
// get by id
interface GetPlayerState {
    getPlayerAttempting: boolean,
    getPlayerError: string,
    getPlayerSuccess: boolean
}

const initialGetPlayerState : GetPlayerState = {
    getPlayerAttempting: false,
    getPlayerError: "",
    getPlayerSuccess: false
}

export const getPlayerReducer = ( state = initialGetPlayerState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_PLAYER_GET_ATTEMPTING:
            return {
                ...state,
                getPlayerAttempting: true,
                getPlayerError: "",
                getPlayerSuccess: false
            }
        case ACTION_PLAYER_GET_SUCCESS:
            return {
                ...state,
                getPlayerAttempting: false,
                getPlayerError: "",
                getPlayerSuccess: true

            }
        case ACTION_PLAYER_GET_ERROR:
            return {
                ...state,
                getPlayerAttempting: false,
                getPlayerError: action.payload,
                getPlayerSuccess: false
            }
        case ACTION_PLAYER_GET_COMPLETE:
            return {
                ...initialGetPlayerState
            }
        default:
            return state;
    }
}

// get all players by gameid
interface GetPlayerInGameState {
    getPlayerInGameAttempting: boolean,
    getPlayerInGameError: string,
    getPlayerInGameSuccess: boolean,
    allPlayers: IPlayer[]
}

const initialGetPlayerInGameState : GetPlayerInGameState = {
    getPlayerInGameAttempting: false,
    getPlayerInGameError: "",
    getPlayerInGameSuccess: false,
    allPlayers: [] 
}

export const getPlayerInGameReducer = ( state = initialGetPlayerInGameState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_PLAYERINGAME_GET_ATTEMPTING:
            return {
                ...state,
                getPlayerInGameAttempting: true,
                getPlayerInGameError: "",
                getPlayerInGameSuccess: false
            }
        case ACTION_PLAYERINGAME_GET_SUCCESS:
            return {
                ...state,
                getPlayerInGameAttempting: false,
                getPlayerInGameError: "",
                getPlayerInGameSuccess: true,
                allPlayers: [...action.payload]
            }
        case ACTION_PLAYERINGAME_GET_ERROR:
            return {
                ...state,
                getPlayerInGameAttempting: false,
                getPlayerInGameError: action.payload,
                getPlayerInGameSuccess: false
            }
        case ACTION_PLAYERINGAME_GET_COMPLETE:
            return {
                ...initialGetPlayerInGameState
            }
        default:
            return state;
    }
}

// delete by id
interface DeletePlayerState {
    deletePlayerAttempting: boolean,
    deletePlayerError: string,
    deletePlayerSuccess: boolean
}

const initialDeletePlayerState : DeletePlayerState = {
    deletePlayerAttempting: false,
    deletePlayerError: "",
    deletePlayerSuccess: false
}

export const deletePlayerReducer = ( state = initialDeletePlayerState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_PLAYER_DELETE_ATTEMPTING:
            return {
                ...state,
                deletePlayerAttempting: true,
                deletePlayerError: "",
                deletePlayerSuccess: false
            }
        case ACTION_PLAYER_DELETE_SUCCESS:
            return {
                ...state,
                deletePlayerAttempting: false,
                deletePlayerError: "",
                deletePlayerSuccess: true

            }
        case ACTION_PLAYER_DELETE_ERROR:
            return {
                ...state,
                deletePlayerAttempting: false,
                deletePlayerError: action.payload,
                deletePlayerSuccess: false
            }
        case ACTION_PLAYER_DELETE_COMPLETE:
            return {
                ...initialDeletePlayerState
            }
        default:
            return state;
    }
}