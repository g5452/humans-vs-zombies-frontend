import RegisterUserForm from "../../components/LandingPageComponents/LoginForm/RegisterUserForm";
import NavigationBar from "../../hoc/NavigationBar";
import { useAuth0 } from "@auth0/auth0-react";
import LoginCheck from "../../hoc/CheckLogin";

const RegisterUserPage = () => {
    return (
        <main>
            <NavigationBar>
            </NavigationBar>

            <RegisterUserForm />
        </main>
    )
}

export default RegisterUserPage;