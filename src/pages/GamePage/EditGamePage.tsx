import { Link, Navigate } from "react-router-dom";
import EditGame from "../../components/AdminComponents/EditGame";
import NavigationBar from "../../hoc/NavigationBar";
import { useAppSelector } from "../../hooks";

const EditGamePage = () => {
    const { admin } = useAppSelector(state => state.session);
    return (
        <main>
            { (admin && 
                <>
                <NavigationBar>
                </NavigationBar>
                <EditGame />
                </>) || 
                <Navigate to="/"></Navigate>
            }
            
        </main>
    )
}

export default EditGamePage;