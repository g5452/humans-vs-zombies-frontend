import { Link } from "react-router-dom";
import Chat from "../../components/ChatComponents/Chat";
import LoginCheck from "../../hoc/CheckLogin";
import NavigationBar from "../../hoc/NavigationBar";

const ChatPage = () => {
    return (
        <>
        <LoginCheck />
        <NavigationBar>
        <Link to="/" className="btn btn-large btn-close btn-close-white" aria-label="Close"></Link>
        </NavigationBar>
        <Chat isSquad={false}/>
        </>
    )
}

export default ChatPage;