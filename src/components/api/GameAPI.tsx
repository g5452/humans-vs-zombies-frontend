import { useAuth0 } from "@auth0/auth0-react";
import http from "../../http-common";
import { IGame, IEditGame } from "../../types/GameTypes";

const getAll = () => {
    return http.get<Array<IGame>>("/Game");
};
const get = (id: any) => {
    return http.get<IGame>(`/game/${id}`);
};
const create = (data: IEditGame) => {
    data.gameState = "Registration";
    return http.post<IGame>("/game", data);
};
const update = (id: any, data: IEditGame) => {
    return http.put<any>(`/game/${id}`, data);
};
const remove = (id: any) => {
    return http.delete<any>(`/game/${id}`);
};
const removeAll = () => {
    return http.delete<any>(`/game`);
};
const findByTitle = (title: string) => {
    return http.get<Array<IGame>>(`/game?title=${title}`);
};



const GameService = {
    getAll,
    get,
    create,
    update,
    remove,
    removeAll,
    findByTitle,
};
export default GameService
