import http from "../../http-common";
import { ISquad, ICreateSquad } from "../../types/SquadTypes";

const getAll = () => {
    return http.get<Array<ISquad>>("/squad");
};
const get = (id: any) => {
    return http.get<ISquad>(`/squad/${id}`);
};

const getSquadByPlayerId = (id: any) => {
    return http.get<any>(`/squad/${id}/player`)
}

const getAllMembers = (id:any) => {
    return http.get<any>(`/squad/${id}/getAllMembersbySquadId`);
};

const getAllSquads = (gameid:any) => {
    return http.get<any>(`/squad/${gameid}/getbyGameId`);
};

const getKilledMembers = (squadid:any) => {
    return http.get<any>(`/squad/${squadid}/getallKilledSquaddMembers`);
};

const create = (data: ICreateSquad, playerID: any) => {
    return http.post<ISquad>(`/squad/${playerID}`, data);
};
const update = (id: any, data: ICreateSquad) => {
    return http.put<any>(`/squad/${id}`, data);
};
const remove = (id: any) => {
    return http.delete<any>(`/squad/${id}`);
};
const removeAll = () => {
    return http.delete<any>(`/squad`);
};

const SquadAPI = {
    getAll,
    get,
    getAllMembers,
    getAllSquads,
    getKilledMembers,
    create,
    update,
    remove,
    removeAll,
    getSquadByPlayerId
};
export default SquadAPI

