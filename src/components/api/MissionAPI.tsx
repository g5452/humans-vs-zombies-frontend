import http from "../../http-common";
import { IMission, IEditMission } from "../../types/MissionTypes";


const getAll = () => {
    return http.get<Array<IMission>>("/mission");
};
const get = (id: any) => {
    return http.get<IMission>(`/mission/${id}`);
};
const getAllByGameId = (gameId: any) => {
    return http.get<Array<IMission>>(`/mission/${gameId}/getbyGameId`);
};
const create = (data: IEditMission) => {
    return http.post<IMission>("/mission", data);
};
const update = (id: any, data: IEditMission) => {
    return http.put<any>(`/mission/${id}`, data);
};
const remove = (id: any) => {
    return http.delete<any>(`/mission/${id}`);
};
const MissionService = {
    getAll,
    get,
    getAllByGameId,
    create,
    update,
    remove,
};
export default MissionService
