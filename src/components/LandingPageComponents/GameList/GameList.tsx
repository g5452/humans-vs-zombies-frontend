import { useEffect, useState } from "react";
import GameListItem from "./GameListItem";
import { IGame } from "../../../types/GameTypes";
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { gamesFetchAttemptAction } from "../../../store/actions/gameActions";
import Error from "../../../hoc/UIMessagesComponents/Error";

const GameList = () => {
    const dispatch = useAppDispatch();
    
    const { getAllAttempting, getAllError, getAllSuccess, games } = useAppSelector(state => state.allGames)

    // will get executed on entering page
    useEffect(()=> {
        if (games.length === 0) dispatch(gamesFetchAttemptAction());}, [])

    return (
        <>
        <main>
            {/* sends data to GameListItem for all games, where all games is displayed */}
            {games?.map((game : IGame) => (
                <div className="card" key={game.id}>  
                    <GameListItem GameID={game.id} name={game.name} status={game.gameState} players={game.players?.length} />
                    <br />
                </div>
                
            ))}
            {/* Attempting to get all games, loading message */}
            { getAllAttempting &&
                <p>loading games...</p>

            }
            {/* Error message, if attempt fails */}
            { getAllError && <Error message={getAllError } />
            }
        </main>
        </>
    )
}

export default GameList;