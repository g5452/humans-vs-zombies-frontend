import { useNavigate } from "react-router-dom";
import "./GameListItem.css"
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { gameFetchAttemptAction } from "../../../store/actions/gameActions";
import { playerInGameGetAttemptAction } from "../../../store/actions/playerActions";


const GameListItem = ({GameID, name, status, players}: any) => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const { loggedIn, admin } = useAppSelector(state => state.session)

    const onClickDetails = () => {
        //get game
        dispatch(gameFetchAttemptAction(GameID));
        localStorage.setItem("gameID", GameID);
        //Attempting to get all players with given game ID
        dispatch( playerInGameGetAttemptAction(GameID) )
        //Navigate to game page
        navigate("/game");
    } 

    return (
        <>
        <div className="card">
            <div className="row">  
                    <div className="card-body game-list-item">
                        <div className="col">
                            <h4 className="card-title mt-1 mb-2"> {name} </h4>
                            <div className="card-text mb-2"> Status: {status} </div>
                            <div className="card-text mb-2"> Players: {players}</div>
                        </div>
                        <div className="col btn-group-sm d-flex mt-2 justify-content-center">
                            { loggedIn &&
                                <button onClick={onClickDetails} className="btn btn-primary btn-sm float-end me-1">Details</button>
                            }
                        </div>
                    </div> 
            </div>
        </div>
        </>
    )

}

export default GameListItem