import { useAuth0 } from "@auth0/auth0-react";
import React from "react";


const LogoutButton = () => {
  const { logout } = useAuth0();

  const onLogout = () => {
    localStorage.clear();
    logout({ returnTo: window.location.origin })
  }

  return (
    <button className="btn btn-primary btn-sm m-1" onClick={onLogout}>
      Logout
    </button>
  );
};

export default LogoutButton;