import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppSelector, useAppDispatch } from '../../../hooks'
import { loginAttemptAction, registerAttemptAction } from '../../../store/actions/userActions';
import { useAuth0 } from "@auth0/auth0-react";
import {IEditUser} from "../../../types/UserTypes"

const LoginForm = () => {
    const dispatch = useAppDispatch();
    
    const { user, loginWithRedirect } = useAuth0();
    const navigate = useNavigate();

    const { loginError } = useAppSelector(state => state.login)

    const login = () => {
        if (user && user.email) {
            localStorage.setItem("Email", user.email);
            dispatch(loginAttemptAction(user.email));
        }
    }

    const register = () => {
        if (loginError) {
            if (user) {
                if (user.email) {
                    let currFirstName = user.given_name
                    let currLastName = user.family_name
    
                    if (!currFirstName || !currLastName) {
                        navigate("/register");
                    }
                    else {
                        const regUser : IEditUser = {
                            firstName : currFirstName,
                            lastName : currLastName,
                            id : user.email,
                            isAdmin : false
                        }
                        dispatch(registerAttemptAction(regUser))
                    }
                }
            }
        }
    }
    
    useEffect(login, [user])

    useEffect(register, [loginError])

    return (
        <>
            <div className="position-absolute end-0">
                <button className="btn btn-primary m-3" onClick={loginWithRedirect}>
                    Login
                </button> 
            </div> 
            
        </>
        
    )
}

export default LoginForm;
