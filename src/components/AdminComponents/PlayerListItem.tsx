import { ChangeEvent, SyntheticEvent, useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { playerGetAttemptAction, playerInGameGetAttemptAction, playerUpdateAttemptAction, playerUpdateCompleteAction } from "../../store/actions/playerActions";
import { IEditPlayer, PlayerState } from "../../types/PlayerTypes";
import Attempting from "../../hoc/UIMessagesComponents/Attempting";
import Success from "../../hoc/UIMessagesComponents/Success";
import Error from "../../hoc/UIMessagesComponents/Error";
import { loadStore } from "../../hoc/LocalStorage";


const PlayerListItem = ({playerID, userID, human, patient}: any) => {

    const dispatch = useAppDispatch();
    const { updatePlayerAttempting, updatePlayerError, updatePlayerSuccess} = useAppSelector(state => state.updatePlayer);

    const [player, setPlayer] = useState<IEditPlayer>({
        isHuman: human,
        isPatientZero: patient
    })
    
    const playerStateOption = [
        {
            label: "-- Change Game Status --",
            value: PlayerState.DEFAULT
        },
        {
            label: "Human",
            value: PlayerState.HUMAN
        },
        {
            label: "Zombie",
            value: PlayerState.ZOMBIE
        }
    ]
    
    const handleSelectInputChange = (event: ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = event.target;
        if(value === PlayerState.HUMAN) {
            setPlayer({ ...player, [name]: true })
        } 
        if(value === PlayerState.DEFAULT) {
            setPlayer({ ...player, [name]: human })
        } 
        if(value === PlayerState.ZOMBIE) {
            setPlayer({ ...player, [name]: false })
        } 
    };

    const submitChangedPlayer = async (event : SyntheticEvent )  => {
        event.preventDefault();
        console.log("Form has been submitted");
        dispatch(playerUpdateAttemptAction(playerID, player))
        
    }
    const getPlayer = () => {
        if(playerID) {
            dispatch(playerGetAttemptAction(playerID));
        }
    }

    // will execute upon loading page
    useEffect(getPlayer, []);

    const playerStatus = () => {
        if (human) return "Human"
        else return "Zombie"
    }

    //Debuging
    // useEffect(()=> {
    //     console.log(playerID, human)
    // });
    useEffect(()=>{
        const gameID = loadStore("gameID");
        if (updatePlayerSuccess) dispatch(playerInGameGetAttemptAction(gameID))
    }, [updatePlayerSuccess])
    
    return (
        <> 
            <div className="card">
                <div className="card-header">
                    <h4>Edit player {userID}</h4>
                </div>
                <div className="card-body">
                    <form onSubmit={submitChangedPlayer}>
                        <div className="form-group">
                            <label htmlFor="name">Current state: {playerStatus()}</label>
                            <select className="form-select mb-3 mt-1 show-menu-arrow" name="isHuman" id="name" onChange={handleSelectInputChange} title="-- Select Player Status --">
                                {playerStateOption.map((option) => (
                                    <option key={option.label} value={option.value}> {option.label}</option>
                                ))}
                            </select>
                            <button className="btn btn-success btn-sm m-1 mb-2" type="submit">
                                Update Player
                            </button>
                        </div>
                    </form>
                        {   updatePlayerAttempting &&
                            <Attempting message={"Updating player..."}/>
                        }
                        { updatePlayerSuccess &&
                            <Success message="Updated player successfully!" path="/editplayers" close="Continue edit players" action={playerUpdateCompleteAction()} home="/game" homeName="Back To GamePage" />        
                        }
                        { updatePlayerError &&
                            <Error message={"Updating player failed: " + updatePlayerError}/>
                        } 
                </div>
            </div>   
            
        </>
    )

}

export default PlayerListItem