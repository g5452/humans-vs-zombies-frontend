import { useNavigate } from "react-router-dom";
import { loadStore, saveStore } from "../../hoc/LocalStorage";
import { useAppDispatch } from "../../hooks";
import { playerInGameGetAttemptAction } from "../../store/actions/playerActions";

const EditPlayersButton = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();


    const editPlayers = () => {
        const path = '/editplayers'
        // const gameID = loadStore("gameID");
        // dispatch(playerInGameGetAttemptAction(gameID))
        navigate(path)
    }

    return (
        <button type="submit" className="btn btn-warning me-1" onClick={editPlayers}>Edit Players</button>
    )


}

export default EditPlayersButton;