import { Navigate } from "react-router-dom";
import CollapseCard from "../../hoc/CollapseCard";
import { useAppSelector } from "../../hooks";
import EditGameButton from "./EditGameButton";
import EditPlayersButton from "./EditPlayersButton";

const AdminGameOptions = () => {
    const { admin } = useAppSelector(state => state.session)

    return (
        <>
            {!admin &&
                <Navigate to="/notaccessible" />
            }
            <CollapseCard title="Administrator Options:" show={true}>
                <EditGameButton />
                <EditPlayersButton />
            </CollapseCard>
        </>
    )
}

export default AdminGameOptions;