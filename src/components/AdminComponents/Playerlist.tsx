import { useAppSelector } from "../../hooks";
import { IPlayer } from "../../types/PlayerTypes";
import PlayerListItem from "./PlayerListItem";

const PlayerList = () => {

    //Get all players from session
    const { allPlayers, getPlayerInGameAttempting, getPlayerInGameError} = useAppSelector(state => state.getPlayersInGame)
   
    return (
        <>
        <div className="card">
            <div className="card-body">
            {/* Attempting to get all players, loading message */}
            { getPlayerInGameAttempting &&
                <p>loading players...</p>

            }
            {/* Error message, if attempt fails */}
            { getPlayerInGameError &&
                <div className='alert alert-danger m-4 p-1'>
                    <h5>Well this is embarrassing...</h5>
                    <p>An error occurred: <br/>{ getPlayerInGameError }</p>
                </div>
            } 
            {/* sends data to PlayerListItem for all players in the game, where all players is displayed */}
            { allPlayers?.map((individualPlayer : IPlayer) => (
                <div key={individualPlayer.id}>
                    <PlayerListItem 
                        playerID={individualPlayer.id}
                        userID={individualPlayer.userId}
                        human={individualPlayer.isHuman}
                        patient={individualPlayer.isPatientZero}
                        />
                </div>
                ))
            }
            </div>
        </div>
        </>
    )
}

export default PlayerList;