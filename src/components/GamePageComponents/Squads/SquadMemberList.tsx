import { useEffect, useState } from "react";
import { Collapse } from "react-bootstrap";
import { setConstantValue } from "typescript";
import { useAppSelector } from "../../../hooks";
import SquadMemberListItem from "./SquadMemberListItem";


const SquadMemberList = () => {
    const { squad } = useAppSelector(state => state.getSquadByPlayerReducer)
    const { allPlayers } = useAppSelector(state => state.getPlayersInGame)
    const { members, getAllMembersInSquadSuccess } = useAppSelector(state => state.getAllMembersInSquad)

    interface ISquadMemberListItem {
        userID: string,
        memberID: number,
        rank: number,
        dead: boolean
    }

    const makeMemberItemList = () => {
        const currentList : ISquadMemberListItem[] = []
        for (const member of members) {
            for (const player of allPlayers) {
                if (member.playerId === player.id) {
                    const squadListItem : ISquadMemberListItem = {
                        userID: player.userId,
                        memberID: member.id,
                        rank: member.rank,
                        dead: !player.isHuman
                    }
                    currentList.push(squadListItem);
                }
            }
        }
        return currentList;
    }

    const [squadMemberItemList, setSquadMemberItemList] = useState(makeMemberItemList());

    const [open, setOpen] = useState(true);

    useEffect(()=>{
        if (getAllMembersInSquadSuccess) {
            setSquadMemberItemList(makeMemberItemList());
        }
    }, [getAllMembersInSquadSuccess])



    return (
        <>
        <div className="card mb-2">
            <div className="card-body">
                <div onClick={()=> setOpen(!open)} 
                    aria-controls="collapse-element"
                    aria-expanded={open}>  
                        <div className="h6 card-header">Members</div>
                </div> 
                <Collapse in={open}>
                    <div id="collapse-element">
                       <table className="table table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">Username</th>
                                    <th scope="col">Rank</th>
                                    {squad.isHuman && <th scope="col">Dead</th>}
                                </tr>
                            </thead>
                            <tbody>
                                { squadMemberItemList?.map((individualMember : ISquadMemberListItem) => (
                                    <tr key={individualMember.memberID}>
                                        <SquadMemberListItem 
                                        id={individualMember.memberID} 
                                        rank={individualMember.rank} 
                                        userID={individualMember.userID} 
                                        humanSquad={squad?.isHuman} 
                                        dead={individualMember.dead} />
                                    </tr>
                                    
                                ))}
                            </tbody>
                        </table> 
                    </div>
                </Collapse>
            </div>
        </div>
        </>
    )
}

export default SquadMemberList;