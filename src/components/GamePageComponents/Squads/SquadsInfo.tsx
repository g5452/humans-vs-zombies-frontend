import { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import CollapseCard from "../../../hoc/CollapseCard";
import { loadStore } from "../../../hoc/LocalStorage";
import Attempting from "../../../hoc/UIMessagesComponents/Attempting";
import Success from "../../../hoc/UIMessagesComponents/Success";
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { squadMemberCreateAttemptingAction, squadMemberCreateCompleteAction } from "../../../store/actions/squadMemberActions";
import { ICreateSquadMember } from "../../../types/SquadMemberTypes";
import Error from "../../../hoc/UIMessagesComponents/Error";

const SquadInfo = ({id, name,  members}: any) => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const {playerId } = useAppSelector(state => state.playerSessionReducer)
    const {createSquadMemberAttempting, createSquadMemberError, createSquadMemberSuccess} = useAppSelector(state => state.createSquadMemberReducer)
    const { Killed } = useAppSelector(state => state.allKilledSessionReducer)
    const gameID = loadStore("gameID");
    

    /**
     * Create new member an joins squad
     */
    const join = () => {
        const member : ICreateSquadMember = {
            rank: 1,
            gameId: gameID,
            squadId: id,
            playerId: playerId
        }
        console.log(member);
        //create member
        dispatch(squadMemberCreateAttemptingAction(member));
        //get all squads in game with created member
        //dispatch(squadsInGameFetchByIdAttemptingAction(gameID));
    }

    const onExit = () => {
        window.location.reload();
        navigate("/squads")
    }

    return (
        <> 

            <div className="row no-gutters fixed-top">
                <div className="col">
                    <div className="alert alert-gradient fade show">
                        <div className="card mx-5 center ">    
                            <div className="nav navbar-nav text-end mt-2">
                                <li><Link to="/squads" className="btn-close btn-close-black btn-lg me-3 text-body" aria-label="Close" onClick={onExit}></Link></li>
                            </div>
                            <h5>{name}</h5>
                            <div className="card-body">
                                    <p>Members: {members} </p>
                                    <p>Deceased Members: {Killed?.length}</p> {/*TODO: FIX*/}
                                    <button className="btn btn-primary w-50 mb-3" onClick={join}>Join</button>
                                    {/* <button onClick={onClickSquadMembers} className="btn btn-outline-dark w-100 fw-bold text-body">Details</button> */}

                                    {  createSquadMemberAttempting &&
                                        <Attempting message={"Joining squad..."}/>
                                    }
                                    
                                    { createSquadMemberError &&
                                        <Error message={"Failed to create new squadmember: " + createSquadMemberError }/>
                                    }  
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </>
    )

}

export default SquadInfo