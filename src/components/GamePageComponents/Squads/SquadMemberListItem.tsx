const SquadMemberListItem = ({id, rank, userID, humanSquad, dead}: any) => {

    const ifDead = () => {
        console.log(dead)
        if (dead) return "Yes"
        else return "No"
    }

    return (
        <> 
        <td>{userID}</td>
        <td>{rank}</td>
        {humanSquad && <td>{ifDead()}</td>}
        </>
    )

}

export default SquadMemberListItem