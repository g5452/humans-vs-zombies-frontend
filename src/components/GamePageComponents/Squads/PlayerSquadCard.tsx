import CollapseCard from "../../../hoc/CollapseCard";
import Error from "../../../hoc/UIMessagesComponents/Error";
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { squadMemberDeleteAttemptAction } from "../../../store/actions/squadMemberActions";
import ChatCard from "../Player/ChatCard";
import SquadMemberList from "./SquadMemberList";

const PlayerSquadCard = () => {
    const { squad } = useAppSelector(state => state.getSquadByPlayerReducer)
    const { members } = useAppSelector(state => state.getAllMembersInSquad)
    const { playerId } = useAppSelector(state => state.playerSessionReducer)
    const { deleteSquadMemberError } = useAppSelector(state => state.deleteSquadMemberReducer)
    const dispatch = useAppDispatch();

    const squadStatus = () => {
        if (squad.isHuman) return "Human"
        else return "Zombie"
    }

    const getSquadMemberID = () : number => {
        for (const member of members) {
            console.log(member)
            if (member.playerId === playerId) {
                return member.id;
            }
        }
        return 0;
    }

    const leaveSquad = () => {
        const memberID = getSquadMemberID();
        if (memberID !== 0) {
            dispatch(squadMemberDeleteAttemptAction(memberID))
        }
        else {
            console.log("memberID not found!")
        }
    }

    return (
        <CollapseCard title={squad.name}>
            <div className="card-text my-1">Type of squad: {squadStatus()}</div>
            <SquadMemberList />
            <button onClick={leaveSquad} className="btn btn-danger my-1">Leave Squad</button>
            {deleteSquadMemberError && <Error message={"Failed to leave squad: " + deleteSquadMemberError} />}
            < ChatCard isSquad={true} title={"Squad Chat"} />
        </CollapseCard>
    )
}

export default PlayerSquadCard;