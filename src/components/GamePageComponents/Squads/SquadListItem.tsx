import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../hooks"
import { killedMembersFetchByIdAttemptingAction, squadFetchByIdAttemptingAction, squadMemberInSquadFetchByIdAttemptingAction, squadsFetchAttemptingAction, squadsInGameFetchByIdAttemptingAction } from "../../../store/actions/squadActions";
import SquadInfo from "./SquadsInfo";

const SquadListItem = ({id, name, squadMembers}: any) => {
    const dispatch = useAppDispatch();
    const [showInfo, SetShowInfo] = useState<boolean>(false)

    const info = () => {
        dispatch ( squadFetchByIdAttemptingAction(id))
        dispatch (killedMembersFetchByIdAttemptingAction(id))
        dispatch( squadMemberInSquadFetchByIdAttemptingAction(id))
        SetShowInfo(true)
    }

    return (
        <> 
            <div className="card mt-2">
                <div className="card-header">
                    <h4>{name}</h4>
                </div>
                <div className="card-body">
                    {!showInfo &&
                        <button className="btn btn-info mb-3 text-white fw-bold" onClick={info}>Show More</button>
                    }
                    {showInfo && 
                        <>
                            <SquadInfo id={id} members= {squadMembers.length} name={name}/>
                        </>
                    }

                </div>
            </div>   
            
        </>
    )

}

export default SquadListItem