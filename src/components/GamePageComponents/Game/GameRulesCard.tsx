import CollapseCard from "../../../hoc/CollapseCard"

const GameRulesCard = () => {
    const gameRuleOverview : string = "Humans vs. Zombies is a game of tag. All players begin as humans, and one is randomly chosen to be the “Original Zombie.” The Original Zombie tags human players and turns them into zombies. Zombies must tag and eat a human every 48 hours or they starve to death and are out of the game."
    const gameRuleObjectiveZ : string = "The Zombies win when all human players have been tagged and turned into zombies."
    const gameRuleObjectiveH : string = "The Humans win by surviving long enough for all of the zombies to starve."

    return (
        <>
        <CollapseCard title="Game Rules" show={true}>
            <div className="card-text"> {gameRuleOverview} </div>
            <br />
            <div className="h5 card-title">Objectives</div>
            <div className="card-text"> {gameRuleObjectiveZ} </div>
            <div className="card-text"> {gameRuleObjectiveH} </div>
        </CollapseCard>
        </>
    )
}

export default GameRulesCard;
