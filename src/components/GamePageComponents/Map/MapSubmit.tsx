import { ReactChild, ReactFragment, ReactPortal, SyntheticEvent, useEffect, useRef, useState } from 'react';

import { useAppDispatch, useAppSelector } from "../../../hooks";
import { missionCreateAttemptAction, missionDeleteAttemptAction, missionsFetchByGameIdAttemptAction } from '../../../store/actions/missionActions';
import { IMission, IEditMission } from '../../../types/MissionTypes';
import DatePicker from 'react-date-picker';
import Map from './Map';
import CollapseCard from '../../../hoc/CollapseCard';


const MapSubmit = () => {
    const dispatch = useAppDispatch();

    const { createMissionSuccess } = useAppSelector(state => state.createMissionReducer);
    const { deleteMissionSuccess } = useAppSelector(state => state.deleteMission);
    const { getAllMissionByGameIdSuccess, gameMissions } = useAppSelector(state => state.allMissionsByGameId);

    const { admin } = useAppSelector(state => state.session);
    const { game } = useAppSelector(state => state.game)
    // Mission POST variables
    const [nameMission, nameMissionOnChange] = useState("");
    const [descMission, descMissionOnChange] = useState("");
    const [gameIdMission, gameIdMissionOnChange] = useState(1);
    const [startDate, startDateOnChange] = useState(new Date());
    const [endDate, endDateOnChange] = useState(new Date());
    const [isHumanVisible, isHumanVisibleOnChange] = useState(true);
    const [isZombieVisible, isZombieVisibleOnChange] = useState(true);
    // Map leaflet marker/current position
    const startPos = () => {
        if ( game && game.se_Lat && game.se_Ing) return [game?.se_Lat, game?.se_Ing] 
        else return [60.391000, 5.327330]
    }
    const [position, setPosition] = useState<any | null>(startPos());

    // All missions in current game
    const [allMissions, setAllMission] = useState<IMission[] | any>()
    // Page reloading check
    const [pageReloadDone, pageReloadDoneOnChange] = useState(false);
    // Set missions to add to DB
    const [mission, setMission] = useState<IEditMission>({
        name: "",
        description: "string",
        isHumanVisible: false,
        isZombieVisible: false,
        startTime: new Date(),
        dateTime: new Date(),
        lat: position.lat,
        ing: position.lng,
        gameId: gameIdMission, 
        
    })
    // Get Missions when done with api call
     useEffect(OnGetMissionSuccess, [getAllMissionByGameIdSuccess]);
    // Get all missions when done with api call
    function OnGetMissionSuccess(){
        setAllMission(gameMissions)
        
    }
    // Get all missions
    function GetAllMissions(){
        
        let data = localStorage.getItem("gameID")
        if(data){
            data = JSON.parse(data)
            const gameId: number = Number(data)
            gameIdMissionOnChange(gameId)
            dispatch(missionsFetchByGameIdAttemptAction(gameId))
        }
    }
    // Delete last mission in missions array
    function DeleteMission(){

        if(allMissions.length > 0) {
            const id: number = allMissions[allMissions.length -1].id
            dispatch(missionDeleteAttemptAction(id))
        }
    }
        
    // will execute when done with api call
    useEffect(GetAllMissions, [deleteMissionSuccess]);
    useEffect(GetAllMissions, [createMissionSuccess]);
    // Prevent POST empty mission to DB on page reload. 
    useEffect(()=>{

        if(pageReloadDone == true){
            dispatch(missionCreateAttemptAction(mission));
        }
        else{
            pageReloadDoneOnChange(true)
        }
      }, [mission]);

      // Submit mission
      const submitChangedMission = (event : SyntheticEvent )  => {
        event.preventDefault();
        setMission({
            name: nameMission, 
            isHumanVisible: isHumanVisible,
            isZombieVisible: isZombieVisible,
            startTime: startDate,
            dateTime: endDate,
            description: descMission,
            lat: position.lat,
            ing: position.lng,
            gameId: gameIdMission,
        })
    }
    
    
    return (
        <div>
            {/* Is Admin? can see submit form */}
            {admin &&
                <CollapseCard title="Add a Mission:">
                    <div className='wrapper'> 
                        <div className="map-form">
                                <form onSubmit={ submitChangedMission}>
                                    <div className="form-group">
                                        <label htmlFor="name">Name:</label>
                                    <input
                                        type="text"
                                        placeholder="name"
                                        className="form-control mb-3 mt-1"
                                        id="name"
                                        onChange={e => nameMissionOnChange(e.target.value)}
                                        name="name"
                                        required
                                    />
                                    <div className='small-elements'>
                                        <>Is human visible  </>
                                        <input 
                                            type="checkbox" 
                                            checked={isHumanVisible}
                                            onChange={e => isHumanVisibleOnChange(e.target.checked)}

                                        />
                                        <> Is Zombie visible  </>
                                        <input 
                                            type="checkbox" 
                                            checked={isZombieVisible}
                                            onChange={e => isZombieVisibleOnChange(e.target.checked)}

                                        />
                                    </div>
                                    <div> 
                                        <> StartDate  </>
                                        <DatePicker onChange={startDateOnChange} value={startDate} />

                                        <> EndDate  </>
                                        <DatePicker onChange={endDateOnChange} value={endDate} />
                                    </div>
                                    
                                    <input
                                        type="text"
                                        placeholder="Description"
                                        className="form-control mb-3 mt-1"
                                        id="name"
                                        onChange={e => descMissionOnChange(e.target.value)}
                                        name="name"
                                    />
                                    <div>
                                        <input
                                            type="number"
                                            disabled
                                            placeholder="Click on map"
                                            className="form-control mb-3 mt-1"
                                            id="name"
                                            name="name"
                                            required
                                        />
                                    </div>
                                </div>
                                <button className="btn btn-success" type="submit">
                                    Submit
                                </button>
                                
                            </form>
                            <button className="btn btn-danger" onClick={ DeleteMission}>
                                    Delete
                                </button>
                        </div>
                        
                    </div>
                </CollapseCard>
            }
            {/* Show map and update mission position */}
            <CollapseCard title="Game Map" show={true}>
                <Map position={position} setPosition={setPosition}></Map>  
            </CollapseCard>
        
        </div>
    )

}
export default MapSubmit;