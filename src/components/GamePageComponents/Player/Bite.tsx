import { SyntheticEvent, useEffect, useState } from "react"
import Error from "../../../hoc/UIMessagesComponents/Error";
import Success from "../../../hoc/UIMessagesComponents/Success";
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { killRegisterAttemptAction, killRegisterCompleteAction } from "../../../store/actions/killActions";
import { IRegisterKill } from "../../../types/KillTypes";

const Bite = () => {
    const dispatch = useAppDispatch();
    const { playerId, gameId } = useAppSelector(state => state.playerSessionReducer)
    const { registerKillSuccess, registerKillAttempting, registerKillError } = useAppSelector(state => state.registerKillReducer)
    const [victimBiteCode, setVictimBiteCode] = useState<string>("");

    const handleBiteFormSubmit = (event: SyntheticEvent) => {
        event.preventDefault();
        // kill
        if (victimBiteCode) {
            const thisKill : IRegisterKill = { gameId : gameId, killerId : playerId, biteCode : victimBiteCode }
            dispatch(killRegisterAttemptAction(thisKill));
        }
        else {
            const error = Error("BiteCode not entered");
            throw error;
        }
    }

    /**
     * Updates bitecode state when typed in.
     * @param event 
     */
    const handleBiteCodeChange = (event: SyntheticEvent) => {
        const input = event.target as HTMLInputElement;
        setVictimBiteCode(input.value.trim());
    };

    /**
     * Clear states after success:
     */
    const clearForm= () => {
        if (registerKillSuccess) {
            console.log("The player was turned to a zombie 😢")
            setVictimBiteCode("");
        }
    }

    // Effects
    useEffect(clearForm, [registerKillSuccess])

    return (
        <div className="card">
            <div className="h5 card-header">MAKE ZOMBIE:</div>
            <div className="card-body">
                <form onSubmit={handleBiteFormSubmit}>
                    <h6>BITE CODE INPUT:</h6>  
                    <div className="d-felx">
                        <input type="text" placeholder="Insert victim's bite code" 
                        onChange={handleBiteCodeChange} name="victimBiteCode" 
                        value={victimBiteCode}
                        id="biteCode" />
                        <button type="submit" className=" btn btn-success btn-sm m-1 mb-2">Validate</button>

                    </div>
                    
                </form>
                { registerKillAttempting && 
                    <div>...attempting to register the kill</div>
                }
                { registerKillSuccess && 
                    <Success message="Registered kill successfully! Ask your victim to refresh their app." home="/game" homeName="Back to Game" action={killRegisterCompleteAction()} hide={true}></Success>
                }
                { registerKillError &&
                    <Error message={"Failed to register bite: " + registerKillError}></Error>
                }
            </div>
            
        </div>
    )
}

export default Bite;