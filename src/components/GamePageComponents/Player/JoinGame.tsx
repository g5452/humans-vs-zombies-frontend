import { useEffect } from "react";
import Error from "../../../hoc/UIMessagesComponents/Error";
import Success from "../../../hoc/UIMessagesComponents/Success";
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { gameFetchAttemptAction } from "../../../store/actions/gameActions";
import { playerCreateAttemptAction, playerCreateCompleteAction } from "../../../store/actions/playerActions";
import { loginAttemptAction } from "../../../store/actions/userActions";
import { RegisterPlayerCredentials } from "../../../types/PlayerTypes";

function JoinGame({userID, gameID} : any) {
    const dispatch = useAppDispatch();
    const { createPlayerAttempting, createPlayerError } = useAppSelector(state => state.createPlayerReducer);

    function join() {
        const player : RegisterPlayerCredentials = {
            isHuman: true,
            isPatientZero: false,
            userId: userID,
            gameId: gameID
        }
        dispatch(playerCreateAttemptAction(player));
    }

    return (
        <>
        <button onClick={join} className="btn btn-success"> Join Game </button>
        { createPlayerAttempting && <p>...joining game</p> }
        { createPlayerError && 
            <Error message={<p>Failed to join game! {createPlayerError}</p>} />
        }
        </>
    )
}
export default JoinGame;